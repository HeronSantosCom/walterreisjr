<?php

class dao_modulo {

    public static function pegar($layout, $usuario_nivel_id) {
        $db = new mysqlsearch();
        $db->table("modulo");
        $db->column("*");
        $db->match("layout", $layout);
        $db->morethan("usuario_nivel_id", $usuario_nivel_id, "AND");
        $db->match("ativo", "1", "AND");
        $dao = $db->go();
        if ($dao) {
            return $dao[0];
        }
        return false;
    }

    public static function listar($usuario_nivel_id) {
        $db = new mysqlsearch();
        $db->table("modulo");
        $db->column("*");
        $db->order("prioridade");
        $db->morethan("usuario_nivel_id", $usuario_nivel_id);
        $db->match("ativo", "1", "AND");
        $dao = $db->go();
        if ($dao) {
            return self::getOrganizarModulos($dao, $usuario_nivel_id);
        }
        return false;
    }

    private static function getOrganizarModulos($array, $usuario_nivel_id) {
        if (is_array($array)) {
            $menu = false;
            foreach ($array as $row) {
                $show = true;
                if ($row["bloqueado"]) {
                    $show = false;
                    if ($usuario_nivel_id == $row["usuario_nivel_id"]) {
                        $show = true;
                    }
                }
                if ($show) {
                    $row["sub"] = false;
                    if (!$row["href"]) {
                        $row["href"] = "javascript:;";
                        if ($row["layout"]) {
                            $row["href"] = "?m={$row["layout"]}";
                        }
                    }
                    $menu[$row["id"]] = $row;
                }
            }
            if ($menu) {
                foreach ($menu as $row) {
                    if ($row["modulo_id"]) {
                        $menu[$row["modulo_id"]]["sub"][$row["id"]] = true;
                    }
                }
                $_ENV["menu"] = $menu;
                $menu = false;
                foreach ($_ENV["menu"] as $idx => $rows) {
                    if (!$rows["modulo_id"]) {
                        $menu[$idx] = self::getSubsModulos($rows["id"]);
                    }
                }
                $_ENV["menu"] = $menu;
            }
        }
        return $_ENV["menu"];
    }

    private static function getSubsModulos($idx) {
        $array = false;
        if (!empty($_ENV["menu"][$idx])) {
            $array = $_ENV["menu"][$idx];
            $subs = (!empty($array["sub"]) ? $array["sub"] : false);
            if ($subs) {
                foreach ($subs as $subs_idx => $value) {
                    $array["sub"][$subs_idx] = self::getSubsModulos($subs_idx);
                }
            }
        }
        return $array;
    }

}