<?php

class dao_comentario {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("comentario");
        $db->column("*");
        $db->match("id", $id);
        $db->match("status", "1", "AND");
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($conteudo_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("comentario");
        $db->column("*");
        if ($conteudo_id) {
            $db->match("conteudo_id", $conteudo_id);
            $db->match("status", "1", "AND");
        } else {
            $db->match("status", "1");
        }
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

    public static function remover($id) {
        $db = new mysqlsave();
        $db->table("comentario");
        $db->column("status", "0");
        $db->match("id", $id);
        if ($db->go()) {
            return true;
        }
        return false;
    }


}