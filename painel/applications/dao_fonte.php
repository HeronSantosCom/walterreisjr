<?php

class dao_fonte {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("fonte");
        $db->column("*");
        $db->match("id", $id);
        $db->match("status", "1", "AND");
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $array = false;
        $db = new mysqlsearch();
        $db->table("fonte");
        $db->column("*");
        $db->match("status", "1");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

    public static function cadastrar($nome, $fonte) {
        if (!self::verificar($nome)) {
            $db = new mysqlsave();
            $db->table("fonte");
            $db->column("nome", $nome);
            $db->column("fonte", $fonte);
            if ($db->go()) {
                return $db->id();
            }
        }
        return false;
    }

    public static function atualizar($id, $nome, $fonte) {
        $db = new mysqlsave();
        $db->table("fonte");
        $db->column("nome", $nome);
        $db->column("fonte", $fonte);
        $db->match("id", $id);
        return $db->go();
    }

    public static function remover($id) {
        $db = new mysqlsave();
        $db->table("fonte");
        $db->column("status", "0");
        $db->match("id", $id);
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function verificar($nome) {
        $db = new mysqlsearch();
        $db->table("fonte");
        $db->column("id");
        $db->match("nome", $nome);
        $db->match("status", "1", "AND");
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

}