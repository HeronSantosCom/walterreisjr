<?php

class layout_colaboradores extends main {

    public function __construct() {
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"]) || isset($_GET["administrar"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["administrar"])) {
                $this->administrar = true;
                if (isset($_POST["id"])) {
                    $this->administrar($_POST["id"]);
                }
            } else {
                if (isset($_GET["remover"])) {
                    $this->remover = true;
                }
                if (isset($_POST["id"])) {
                    $this->salvar($_POST["id"]);
                }
            }
        }
        $this->usuarios = dao_usuario::listar(logon::meu_id(), 3);
    }

    private function abrir($id) {
        $dao = (dao_usuario::pegar($id, logon::meu_id()));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function administrar($id) {
        $_SESSION["saved_logon"][] = logon::meu_id();
        if (logon::reboot($id, false, false, false)) {
            knife::redirect("/index.html");
        }
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("A combinação das senhas inseridas não conferem.");
            if ($this->senha == $this->senha_confirm) {
                $this->msgbox("Não foi possível salvar o colaborador, verifique os dados e tente novamente!");
                if ($id) {
                    $action = dao_usuario::atualizar($id, $this->nome, $this->email, $this->senha, 3);
                    if ($action) {
                        spool::usuario_atualizado($this->nome, $this->email, $this->senha);
                    }
                } else {
                    $action = dao_usuario::cadastrar($this->nome, $this->email, $this->senha, 3, logon::meu_id());
                    if ($action) {
                        spool::usuario_cadastrado($this->nome, $this->email, $this->senha);
                    }
                }
                if ($action) {
                    unset($this->formulario);
                    $this->msgbox("Colaborador salvo com sucesso!");
                }
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o colaborador, tente novamente!");
        if ($id) {
            $action = dao_usuario::remover($id, $this->motivo, false);
            if ($action) {
                spool::usuario_removido($this->nome, $this->email, $this->telefone, $this->motivo);
                unset($this->formulario);
                $this->msgbox("Colaborador removido com sucesso!");
            }
        }
    }

}