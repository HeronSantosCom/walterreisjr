<?php

class layout_fontes extends main {

    public function __construct() {
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
        }
        $this->fontes = dao_fonte::listar();
    }

    private function abrir($id) {
        $dao = (dao_fonte::pegar($id));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar a fonte, verifique os dados e tente novamente!");
            if ($id) {
                $action = dao_fonte::atualizar($id, $this->nome, $this->fonte);
            } else {
                $action = dao_fonte::cadastrar($this->nome, $this->fonte);
            }
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Fonte salva com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover a fonte, tente novamente!");
        if ($id) {
            $action = dao_fonte::remover($id);
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Fonte removida com sucesso!");
            }
        }
    }

}