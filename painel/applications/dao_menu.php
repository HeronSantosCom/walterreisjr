<?php

class dao_menu {

    public static function pegar($id) {
        $db = new mysqlsearch();
        $db->table("menu");
        $db->column("*");
        $db->match("id", $id);
        $db->match("status", "1", "AND");
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar() {
        $array = false;
        $db = new mysqlsearch();
        $db->table("menu");
        $db->column("*");
        $db->match("status", "1");
        $db->order("ordenacao");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        return $row;
    }

    public static function cadastrar($alias, $nome, $ordenacao, $destaque) {
        if (!self::verificar($alias)) {
            $db = new mysqlsave();
            $db->table("menu");
            $db->column("alias", $alias);
            $db->column("nome", $nome);
            $db->column("ordenacao", $ordenacao);
            $db->column("destaque", $destaque);
            if ($db->go()) {
                return $db->id();
            }
        }
        return false;
    }

    public static function atualizar($id, $alias, $nome, $ordenacao, $destaque) {
        $db = new mysqlsave();
        $db->table("menu");
        $db->column("alias", $alias);
        $db->column("nome", $nome);
        $db->column("ordenacao", $ordenacao);
        $db->column("destaque", $destaque);
        $db->match("id", $id);
        return $db->go();
    }

    public static function remover($id) {
        $db = new mysqlsave();
        $db->table("menu");
        $db->column("status", "0");
        $db->match("id", $id);
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function verificar($alias) {
        $db = new mysqlsearch();
        $db->table("menu");
        $db->column("id");
        $db->match("alias", $alias);
        $db->match("status", "1", "AND");
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

}