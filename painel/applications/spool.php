<?php

class spool extends main {

    public static function usuario_cadastrado($nome, $email, $senha) {
        return spool_email::usuario_cadastrado($nome, $email, $senha);
    }

    public static function usuario_atualizado($nome, $email, $senha) {
        return spool_email::usuario_atualizado($nome, $email, $senha);
    }

    public static function usuario_removido($nome, $email) {
        return spool_email::usuario_removido($nome, $email);
    }

    public static function usuario_movido($nome, $email) {
        return spool_email::usuario_movido($nome, $email);
    }

    public static function usuario_senha_redefinida($nome, $email, $senha) {
        return spool_email::usuario_senha_redefinida($nome, $email, $senha);
    }

}