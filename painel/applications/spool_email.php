<?php

class spool_email {

    public static function usuario_cadastrado($nome, $email, $senha) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Seja bem-vindo ao " . name . "!";
        $mensagem[] = "";
        $mensagem[] = "Abaixo segue sua credencial de acesso, por segurança anote em um local seguro.";
        $mensagem[] = "";
        $mensagem[] = "\tSeu usuário é: {$email}";
        $mensagem[] = "\tSua senha é: {$senha}";
        $mensagem[] = "\tEndereço de acesso ao painel: http://" . domain . "/";
        return self::envia_email($email, "Seja bem-vindo!", $mensagem);
    }

    public static function usuario_atualizado($nome, $email, $senha) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Informamos que seus dados foram atualizados com sucesso!";
        $mensagem[] = "Por segurança, anote em um local seguro.";
        $mensagem[] = "";
        $mensagem[] = "\tSeu usuário é: {$email}";
        if ($senha) {
            $mensagem[] = ($senha ? "\tSua senha é: {$senha}" : "\tSua senha não foi alterada!");
        }
        $mensagem[] = "\tEndereço de acesso: http://" . domain . "/";
        return self::envia_email($email, "Suas informações foram atualizadas!", $mensagem);
    }

    public static function usuario_removido($nome, $email) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Informamos que sua conta foi removida dos nossos serviços!";
        $mensagem[] = "";
        $mensagem[] = "\tSeu usuário era: {$email}";
        return self::envia_email($email, "Suas informações foram atualizadas!", $mensagem);
    }

    public static function usuario_movido($nome, $email) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Informamos que seu gestor moveu alguns clientes e planos para sua conta!";
        return self::envia_email($email, "Suas informações foram atualizadas!", $mensagem);
    }

    public static function usuario_senha_redefinida($nome, $email, $senha) {
        $mensagem[] = "Olá {$nome},";
        $mensagem[] = "";
        $mensagem[] = "Como solicitado, estamos enviando sua nova senha de acesso!";
        $mensagem[] = "Por segurança, anote em um local seguro.";
        $mensagem[] = "";
        $mensagem[] = "\tSua nova senha é: {$senha}";
        $mensagem[] = "\tEndereço de acesso: http://" . domain . "/";
        $mensagem[] = "";
        $mensagem[] = "Esta é uma senha gerada aleatóriamente, por favor, altere!";
        return self::envia_email($email, "Sua senha foi redefinida!", $mensagem);
    }

    private static function envia_email($email, $titulo, $mensagem) {
        $mensagem[] = "";
        $mensagem[] = "Lembre-se, em caso de dúvidas ou sugestões entre em contato conosco através dos nossos canais de atendimento!";
        $mensagem[] = "";
        $mensagem[] = name;
        $mensagem[] = "http://" . domain . "/";
        $headers = "From: " . name . " <contato@" . domain . ">\n";
        $headers .= "Reply-To: " . name . " <contato@" . domain . ">\n";
        return knife::mail_utf8($email, '[' . name . '] ' . $titulo, htmlspecialchars(join("\n", $mensagem)), $headers);
    }

}