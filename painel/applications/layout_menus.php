<?php

class layout_menus extends main {

    public function __construct() {
        if (isset($_GET["cadastrar"]) || isset($_GET["editar"]) || isset($_GET["remover"])) {
            $this->formulario = true;
            if (!empty($_GET["id"])) {
                if (!$this->abrir($_GET["id"])) {
                    define("app_layout_error", true);
                    return false;
                }
            }
            if (isset($_GET["remover"])) {
                $this->remover = true;
            }
            if (isset($_POST["id"])) {
                $this->salvar($_POST["id"]);
            }
        }
        $this->menus = dao_menu::listar();
    }

    private function abrir($id) {
        $dao = (dao_menu::pegar($id));
        if ($dao) {
            $this->extract($dao);
            return true;
        }
        return false;
    }

    private function salvar($id) {
        $this->extract($_POST);
        if ($this->remover) {
            if ($id) {
                $this->remover($_POST["id"]);
            }
        } else {
            $this->msgbox("Não foi possível salvar o menu, verifique os dados e tente novamente!");
            if ($id) {
                $action = dao_menu::atualizar($id, $this->alias, $this->nome, $this->ordenacao, $this->destaque);
            } else {
                $action = dao_menu::cadastrar($this->alias, $this->nome, $this->ordenacao, $this->destaque);
            }
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Menu salvo com sucesso!");
            }
        }
    }

    private function remover($id) {
        $this->msgbox("Não foi possível remover o menu, tente novamente!");
        if ($id) {
            $action = dao_menu::remover($id);
            if ($action) {
                unset($this->formulario);
                $this->msgbox("Menu removido com sucesso!");
            }
        }
    }

}