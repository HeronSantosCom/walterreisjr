<?php

class layout_minha_conta extends main {

    public function __construct() {
        if (isset($_POST["email"])) {
            $this->salvar();
        }
        $this->extract(self::session());
    }

    private function salvar() {
        $this->extract($_POST);
        $this->msgbox("A combinação das senhas inseridas não conferem.");
        if ($this->senha == $this->senha_confirm) {
            $this->msgbox("Não foi possível alterar sua conta, verifique os dados e tente novamente.");
            $action = dao_usuario::atualizar(logon::meu_id(), $this->nome, $this->email, $this->senha, logon::meu_usuario_nivel_id());
            if ($action) {
                self::reboot(logon::meu_id(), false, false, (self::cookie() ? '1' : '0'));
                $this->msgbox("Sua conta foi alterada com sucesso.");
            }
        }
    }

}