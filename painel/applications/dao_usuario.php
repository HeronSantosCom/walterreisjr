<?php

class dao_usuario {

    public static function pegar($id, $usuario = false, $senha = false, $usuario_id = false) {
        $db = new mysqlsearch();
        $db->table("usuario");
        $db->column("*");
        if ($id) {
            $db->match("id", $id);
        } else {
            $db->match("email", $usuario);
            $db->match("senha", md5($senha), "AND");
        }
        if ($usuario_id) {
            $db->match("usuario_id", $usuario_id, "AND");
        }
        $db->match("status", "1", "AND");
        $dao = $db->go();
        if (!empty($dao[0])) {
            return self::hook($dao[0]);
        }
        return false;
    }

    public static function listar($usuario_id, $usuario_nivel_id) {
        $array = false;
        $db = new mysqlsearch();
        $db->table("usuario");
        $db->column("*");
        $db->match("usuario_id", $usuario_id);
        $db->match("usuario_nivel_id", $usuario_nivel_id, "AND");
        $db->match("id", $usuario_id, "AND", true);
        $db->match("status", "1", "AND");
        $dao = $db->go();
        if ($dao) {
            foreach ($dao as $row) {
                $array[$row["id"]] = self::hook($row);
            }
        }
        return $array;
    }

    private static function hook($row) {
        $row["gravatar"] = knife::gravatar($row["email"], 360);
        return $row;
    }

    public static function cadastrar($nome, $email, $senha, $usuario_nivel_id, $usuario_id) { // $endereco, $cep,
        if (!self::verificar($email)) {
            $db = new mysqlsave();
            $db->table("usuario");
            $db->column("nome", $nome);
            $db->column("email", $email);
            $db->column("senha", md5($senha));
            $db->column("usuario_nivel_id", $usuario_nivel_id);
            $db->column("usuario_id", $usuario_id);
            if ($db->go()) {
                return $db->id();
            }
        }
        return false;
    }

    public static function atualizar($id, $nome, $email, $senha, $usuario_nivel_id) {
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("nome", $nome);
        $db->column("email", $email);
        if ($senha) {
            $db->column("senha", md5($senha));
        }
        $db->column("usuario_nivel_id", $usuario_nivel_id);
        $db->match("id", $id);
        return $db->go();
    }

    public static function redefinir_senha($email, $senha) {
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("senha", md5($senha));
        $db->match("email", $email);
        return $db->go();
    }

    public static function remover($id) {
        $db = new mysqlsave();
        $db->table("usuario");
        $db->column("status", "0");
        $db->match("id", $id);
        if ($db->go()) {
            return true;
        }
        return false;
    }

    public static function verificar($email) {
        $db = new mysqlsearch();
        $db->table("usuario");
        $db->column("id");
        $db->match("email", $email);
        $db->match("status", "1", "AND");
        $dao = $db->go();
        if (!empty($dao[0])) {
            return true;
        }
        return false;
    }

}