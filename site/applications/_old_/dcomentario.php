<?php

class dComentario {

    static function Lista($conteudo_id) {
        $db = new mysqlsearch();
        $db->table("v_comentario");
        $db->column("id");
        $db->column("nome");
        $db->column("comentario");
        $db->column("email");
        $db->column("ip");
        $db->column("atualizacao");
        $db->column("atualizacao_br");
        $db->match("conteudo_id", $conteudo_id);
        $db->order(7, "ASC");
        return $db->go();
    }

    static function Comentar($nome, $email, $comentario, $conteudo_id) {
        $db = new mysqlsave();
        $db->table("comentario");
        $db->column("nome", $nome);
        $db->column("comentario", $comentario);
        $db->column("email", $email);
        $db->column("conteudo_id", $conteudo_id);
        return $db->go();
    }

}