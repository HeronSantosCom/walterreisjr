<?php

class sindex extends app {

    public function __construct() {
        if ($this->sitemap)
            die($this->sSitemap());

        if (!$this->menu)
            $this->menu = "index";

        $this->sCabecalho(name, "Site pessoal do candidato a vereador para a prefeitura de Nova Iguaçu.", "Heron Santos");

        if ($this->menu)
            $this->sMenu();

        if (!$this->id)
            switch ($this->menu) {
                case "index":
                    $this->oIndex();
                    break;
                case "participe":
                    $this->oParticipe();
                    break;
                case "buscador":
                    $this->menu_titulo = "Buscador";
                    $this->oListar($this->q, false, false);
                    break;
                default:
                    $this->oListar($this->q, $this->tipo, $this->menu);
                    break;
            }

        if ($this->id)
            $this->oAbrir();

        $this->interno_menu = $this->gMenu();
        $this->interno_lateral_menu = $this->gMenuLateral();
        $this->corpo = knife::html($this->layout);
    }

    private function oIndex($limite = 5) {
        $this->lista_destaque = dConteudo::Lista(false, false, false, false, $limite, true);
        $this->gListar(false, false, false, $limite);
        $this->layout = "layout/corpo/index.html";
    }

    public function oListar($busca = false, $tipo = false, $menu = false) {
        $this->total = dConteudo::Total($busca, $tipo, $menu);
        $this->gListar($busca, $tipo, $menu);
        $this->layout = "layout/corpo/listar.html";
    }

    private function oParticipe() {
        $this->sCabecalho($this->titulo, "Venha comigo nessa jornada rumo a vitória!");
        $recaptcha = new recaptcha();
        if (isset($_POST["submit"])) {
            if ($recaptcha->validacao()) {
                $this->extract($_POST);
                $email[] = "";
                $email[] = "Mensagem";
                $email[] = "=============================";
                $email[] = "Nome: {$this->author}";
                $email[] = "E-mail: {$this->email}";
                $email[] = "Mensagem: {$this->comment}";
                $email[] = "";
                $email[] = "Informações";
                $email[] = "=============================";
                $email[] = "Data: " . date("r");
                $email[] = "IP: {$_SERVER["REMOTE_ADDR"]}";
                $email[] = "";
                $name = htmlspecialchars($this->author);
                $mailFrom = htmlspecialchars($this->email);
                $headers = "From: $name <$mailFrom>\n";
                $headers .= "Reply-To: $name <$mailFrom>\n";
                if (!mail('heronsantos@outlook.com', 'Contato do Site', htmlspecialchars(join("\n", $email)), $headers)) {
                    $this->msg_erro = "Não foi possível enviar sua mensagem no momento, tente novamente mais tarde!";
                } else {
                    $this->msg_erro = "Mensagem foi enviada com sucesso!";
                }
            } else
                $this->msg_erro = "O código não é válido!";
        }
        $this->recaptcha = $recaptcha->form();
        $this->layout = "layout/corpo/participe.html";
    }

    private function oAbrir() {
        $conteudo = dConteudo::Pega($this->id);
        if ($conteudo) {
            $this->extract($conteudo[0]);
            $this->sCabecalho("{$this->menu} - {$this->titulo}", $this->descricao, $this->usuario);
            dConteudo::PageView($this->id);
            $this->sComentario();
            $this->gComentario();
            $this->layout = "layout/corpo/abrir.html";
        } else {
            $this->layout = "layout/corpo/erro.html";
        }
    }

    public function gListar($busca = false, $tipo = false, $menu = false, $limite = 8) {
        $this->extract($_POST);
        $this->sPaginacao(($this->pagina_atual ? $this->pagina_atual : 1), $this->total, $limite);
        $this->lista = dConteudo::Lista($busca, $tipo, $menu, $this->pagina_primeiro_item, $this->pagina_limite, false);
        if (!$this->q and count($this->lista) == 1)
            $this->id = $this->lista[0]["id"];
        $this->rss = "http://" . domain . "/rss.xml?" . $_SERVER["QUERY_STRING"];
    }

    private function sPaginacao($page, $rows, $limit) {
        $this->pagina_atual = $page;
        $this->pagina_itens = $rows;
        $this->pagina_limite = $limit;
        if ($this->pagina_itens > 0) {
            $this->pagina_total = ceil($this->pagina_itens / $this->pagina_limite);
            if ($this->pagina_atual > $this->pagina_total) {
                $this->pagina_atual = $this->pagina_total;
            }
            if ($this->pagina_atual > 1) {
                $this->pagina_primeira = 1;
                $this->pagina_anterior = ($this->pagina_atual - 1);
            }
            if ($this->pagina_atual < $this->pagina_total and $this->pagina_total > 1) {
                $this->pagina_proxima = ($this->pagina_atual + 1);
                $this->pagina_ultima = $this->pagina_total;
            }
            $this->pagina_primeiro_item = (($this->pagina_limite * $this->pagina_atual) - $this->pagina_limite);
            $this->pagina_ultimo_item = ($this->pagina_itens > $this->pagina_limite * $this->pagina_atual ? ($this->pagina_limite * $this->pagina_atual) - 1 : $this->pagina_itens - 1);
        }
    }

    private function gComentario() {
        $comentarios = dComentario::Lista($this->id);
        if ($comentarios) {
            foreach ($comentarios as $key => $value) {
                $comentarios[$key]["avatar"] = knife::gravatar($value["email"], 32);
            }
        }
        $this->comentarios = $comentarios;
    }

    private function sMenu() {
        $menu = dMenu::Pega(false, $this->menu);
        if ($menu) {
            $this->menu_titulo = $menu[0]["nome"];
            $this->menu_alias = $menu[0]["alias"];
        }
    }

    private function sComentario() {
        $recaptcha = new recaptcha();
        if (isset($_POST["submit"])) {
            if ($recaptcha->validacao()) {
                $this->extract($_POST);
                if (dComentario::Comentar($this->author, $this->email, $this->comment, $this->id)) {
                    $this->msg_erro = "Comentário feito com sucesso!";
                } else {
                    $this->msg_erro = "Não foi possível salvar seu comentário no momento, tente novamente mais tarde!";
                }
            } else {
                $this->msg_erro = "O código não é válido!";
            }
        }
        $this->recaptcha = $recaptcha->form();
    }

    private function gMenu() {
        return dMenu::Lista(true);
    }

    private function gMenuLateral() {
        return dMenu::Lista();
    }

    private function sCabecalho($title = false, $description = false, $author = false, $rss = false) {
        if ($title)
            $this->index_title = $title;
        if ($description)
            $this->index_description = $description;
        if ($author)
            $this->index_author = $author;
    }

    private function sSitemap($sitemap = false, $total = false, $inicial = 0) {
        if (!$inicial and !$total and !$sitemap) {
            if (!class_exists("sitemap", false))
                include path::plugins("insigndigital/sitemap.php");
            $db = new mysqlsearch();
            $db->table("v_conteudo");
            $db->column("COUNT(id)", false, "total");
            $total = $db->go();
            $total = $total[0]["total"];
            $sitemap = new Sitemap();
        }
        if ($total > 0 and $inicial < $total) {
            $db = new mysqlsearch();
            $db->table("v_conteudo");
            $db->column("id");
            $db->column("alias");
            $db->limit($inicial, 1000);
            $conteudos = $db->go();
            if (is_array($conteudos)) {
                foreach ($conteudos as $conteudo) {
                    $sitemap->url("http://" . domain . "/index.html?menu={$conteudo["alias"]}&id={$conteudo["id"]}", false, "always");
                }
            }
            $this->sSitemap($sitemap, $total, ($inicial + 1000));
        }
        if (!$inicial) {
            return $sitemap->go();
        }
    }

}