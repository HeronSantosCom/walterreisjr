<?php

class dMenu {

    static function Pega($id = false, $alias = false) {
        $db = new mysqlsearch();
        $db->table("menu");
        $db->column("*");
        if ($id) {
            $db->match("id", $id);
        } else {
            if ($alias) {
                $db->match("alias", $alias);
            }
        }
        return $db->go();
    }

    static function Lista($destaque = false) {
        $db = new mysqlsearch();
        $db->table("menu");
        $db->column("id");
        $db->column("alias");
        $db->column("nome");
        $db->column("ordenacao");
        $db->column("destaque");
        $db->column("atualizacao");
        $db->column("status");
        $db->match("status", "1");
        if ($destaque) {
            $db->match("destaque", $destaque);
        }
        return $db->go();
    }

}