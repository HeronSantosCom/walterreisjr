<?php

if (!class_exists("PhpThumb", false))
    include path::plugins("phpthumb/ThumbLib.inc.php");

class render extends app {

    public function __construct() {
        $this->extract($_GET);
        if ($this->image)
            $this->gImage($this->image, ($this->w ? $this->w : 128), ($this->h ? $this->h : ($this->w ? $this->w : 128)));
    }

    private function sFolder($width, $heigth, $recursive = 0) {
        $folder = path::sources("media/imagem/{$width}x{$heigth}");
        if (!$folder) {
            mkdir(path::sources("media/imagem") . "/{$width}x{$heigth}");
            return ($recursive < 3 ? $this->sFolder($width, $heigth, ($recursive + 1)) : false);
        }
        return $folder;
    }

    private function sImage($image, $width, $heigth, $recursive = 0) {
        $filename = path::sources("media/imagem/{$image}");
        if ($filename) {
            $filename_rendered = $this->sFolder($width, $heigth) . "/{$image}";
            if (!file_exists($filename_rendered)) {
                $pathinfo = pathinfo($filename);
                if (!empty($pathinfo["extension"])) {
                    $thumb = PhpThumbFactory::create($filename);
                    $thumb->adaptiveResize($width, $heigth);
                    $thumb->save($filename_rendered, $pathinfo["extension"]);
                    return ($recursive < 3 ? $this->sImage($image, $width, $heigth, ($recursive + 1)) : false);
                }
            }
            return $filename_rendered;
        }
        return $filename;
    }

    private function gImage($image, $width, $heigth) {
        $image = str_replace(path::sources(), "", $this->sImage($image, $width, $heigth));
        //die($image);
        knife::redirect($image);
    }

}