<?php

if (!function_exists("_recaptcha_qsencode")) {
    include path::plugins("recaptchalib.php");
}

class recaptcha extends app {

    static function form() {
        ob_start();
        $publickey = "6LfUN9USAAAAAKGHw5zf91q3LnqHA5qv-s-XY5uB"; // you got this from the signup page
        echo recaptcha_get_html($publickey);
        return ob_get_clean();
    }

    static function validacao() {
        $privatekey = "6LfUN9USAAAAAKUj6PI9nwCfDuNxbqilNfNtErC3";
        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        return $resp->is_valid;
    }

}

?>