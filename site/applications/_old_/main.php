<?php

class main extends app {

    public function __construct() {
        $this->extract($_GET);

        switch (subdomain) {
            case "admin":
                new sadmin();
                break;
            default:
                new sindex();
                break;
        }
    }

}