<?php

class dConteudo {

    static function Pega($id) {
        $db = new mysqlsearch();
        $db->table("v_conteudo");
        $db->column("*");
        $db->match("id", $id);
        return $db->go();
    }

    static function Total($indice = false, $tipo = false, $alias = false, $inicial = false, $limite = false) {
        $db = new mysqlsearch();
        $db->table("v_conteudo");
        $db->column("COUNT(id)", false, "total");
        $glue = false;
        if ($indice) {
            $db->against(array("indice"), $indice);
            $glue = true;
        }
        if ($tipo) {
            $db->match("tipo", $tipo, ($glue ? "AND" : false));
            $glue = true;
        }
        if ($alias)
            $db->match("alias", $alias, ($glue ? "AND" : false));
        $total = $db->go();
        if ($total)
            $total = $total[0]["total"];
        return $total;
    }

    static function Lista($indice = false, $tipo = false, $alias = false, $inicial = false, $limite = 10, $destaque = false) {
        $db = new mysqlsearch();
        $db->table("v_conteudo");
        $db->column("id");
        $db->column("tipo");
        $db->column("titulo");
        $db->column("descricao");
        $db->column("conteudo");
        $db->column("chamada");
        $db->column("pageview");
        $db->column("atualizacao");
        $db->column("usuario");
        $db->column("menu");
        $db->column("alias");
        $db->column("comentarios");
        $db->column("atualizacao_br");
        $db->column("atualizacao_rss");
        $db->column("indice");
        $glue = false;
        if ($indice) {
            $db->against(array("indice"), $indice);
            $glue = true;
        }
        if ($tipo) {
            $db->match("tipo", $tipo, ($glue ? "AND" : false));
            $glue = true;
        }
        if ($alias) {
            $db->match("alias", $alias, ($glue ? "AND" : false));
            $glue = true;
        }
        if ($destaque) {
            $db->is("chamada", false, ($glue ? "AND" : false), true);
            $glue = true;
        }
        if ($limite) {
            $db->limit($limite);
            if ($inicial)
                $db->limit($inicial, $limite);
        }
        $db->order(($destaque ? 7 : 8), "DESC");
        return $db->go();
    }

    static function PageView($id) {
        $db = new mysqlsave();
        $db->table("conteudo");
        $db->column("pageview", "IF(pageview = 0, 1, pageview + 1)");
        $db->match("id", $id);
        return $db->go();
    }

}