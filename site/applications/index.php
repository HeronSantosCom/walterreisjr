<?php

class index extends app {

    public function __construct() {
        $permalink = permalink::get(0);
        knife::dump($permalink);
        $layout = ($permalink ? $permalink : "index");
        switch ($layout) {
            case 'contato':
                $this->contato();
                break;
        }
        $this->active = $layout;
        $this->modulo("layout/main/{$layout}.html");
    }

    protected function modulo($modulo) {
        $this->layout = knife::html($modulo);
    }

    public function __call($name, $arguments) {
        return false;
    }

    public function contato() {
        if (isset($_POST["email"])) {
            $this->extract($_POST);
            $this->nome = htmlspecialchars($this->nome);
            $this->email = htmlspecialchars($this->email);
            $this->assunto = htmlspecialchars($this->assunto);
            $this->mensagem = str_replace(array("\t", "\r", "\n"), " ", $this->mensagem);
            if (self::atendimento($this->nome, $this->email, $this->assunto, $this->mensagem)) {
                $this->mensagem_enviada = true;
            }
        }
    }

    public static function atendimento($nome, $email, $assunto, $mensagem) {
        $corpo_gestor[] = date("r") . ",";
        $corpo_gestor[] = $assunto;
        $corpo_gestor[] = "";
        $corpo_gestor[] = $mensagem;
        $corpo_gestor[] = "";
        $corpo_gestor[] = "Atenciosamente,";
        $corpo_gestor[] = "{$nome} ({$email})";
        $corpo_gestor[] = "Atendimento: " . getmypid() . "({$_SERVER["REMOTE_ADDR"]})";
        if (self::envia_para_gestor($nome, $email, "Novo Contato do Site (" . getmypid() . ")", $corpo_gestor)) {
            $corpo_cliente[] = "Olá {$nome},";
            $corpo_cliente[] = "";
            $corpo_cliente[] = "Informamos sua mensagem que foi registrada sob o número " . getmypid() . " e em breve retornaremos!";
            $corpo_cliente[] = "";
            $corpo_cliente[] = "========================";
            $corpo_cliente[] = date("r") . ",";
            $corpo_cliente[] = $assunto;
            $corpo_cliente[] = "";
            $corpo_cliente[] = $mensagem;
            $corpo_cliente[] = "";
            $corpo_cliente[] = "Atenciosamente,";
            $corpo_cliente[] = "{$nome} ({$email})";
            $corpo_cliente[] = "========================";
            return self::envia_para_cliente($email, "Sua mensagem foi recebida! (" . getmypid() . ")", $corpo_cliente);
        }
        return false;
    }

    private static function envia_para_cliente($email, $titulo, $mensagem) {
        $mensagem[] = "";
        $mensagem[] = name;
        $mensagem[] = "http://www.walterreisjr.com.br/";
        $headers = "From: " . name . " <contato@walterreisjr.com.br>\n";
        $headers .= "Reply-To: " . name . " <contato@walterreisjr.com.br>\n";
        return knife::mail_utf8($email, '[' . name . '] ' . $titulo, htmlspecialchars(join("\n", $mensagem)), $headers);
    }

    private static function envia_para_gestor($nome, $email, $titulo, $mensagem) {
        $mensagem[] = "";
        $mensagem[] = name;
        $mensagem[] = "http://www.walterreisjr.com.br/";
        $headers = "From: " . $nome . " <" . $email . ">\n";
        $headers .= "Reply-To: " . $nome . " <" . $email . ">\n";
        return knife::mail_utf8("contato@walterreisjr.com.br", '[' . name . '] ' . $titulo, htmlspecialchars(join("\n", $mensagem)), $headers);
    }

}

?>
