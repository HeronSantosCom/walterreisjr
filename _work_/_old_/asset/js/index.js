var index = {
    
    param: function(name) {
        return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]);
    },
    
    audio: function() {
        $("#audio_jplayer").jPlayer({
            ready: function (event) {
                $(this).jPlayer("setMedia", {
                    mp3: $("#audio").attr("rel")
                });
            },
            swfPath: "js",
            supplied: "mp3",
            wmode: "window"
        });
    },
    
    video: function() {
        $("#video_jplayer").jPlayer({
            ready: function () {
                $(this).jPlayer("setMedia", {
                    m4v: $("#video").attr("rel"),
                    poster: $("#video").attr("poster")
                });
            },
            swfPath: "js",
            supplied: "m4v",
            size: {
                width: "480px",
                height: "270px",
                cssClass: "jp-video-270p"
            }
        });
    },
    
    is_email: function(email) {
        return /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email);
    },
    
    paginacao: function() {
        $('#listagem').scrollPagination({
            'contentPage': 'paginacao/conteudo.html' + location.search,
            'contentData': {
                'pagina_atual' : parseInt($('#pagina_proxima').val())
            },
            'scrollTarget': $(window),
            'heightOffset': 10,
            'afterLoad': function(){
                var pagina_total = parseInt($('#pagina_total').val());
                var pagina_proxima = parseInt($('#pagina_proxima').val());
                if (pagina_proxima < pagina_total) {
                    this.contentData = {
                        'pagina_atual' : (pagina_proxima + 1)
                    };
                    $('#pagina_proxima').val(pagina_proxima + 1);
                } else {
                    $('#listagem').stopScrollPagination();
                }
            }
        });
    },
    
    slider: function() {
        $("#myController").jFlow({
            controller: ".jFlowControl", // must be class, use . sign
            slideWrapper : "#jFlowSlider", // must be id, use # sign
            slides: "#mySlides",  // the div where all your sliding divs are nested in
            selectedWrapper: "jFlowSelected",  // just pure text, no sign
            effect: "flow", //this is the slide effect (rewind or flow)
            width: "570px",  // this is the width for the content-slider
            height: "280px",  // this is the height for the content-slider
            duration: 400,  // time in milliseconds to transition one slide
            pause: 5000, //time between transitions
            prev: ".jFlowPrev", // must be class, use . sign
            next: ".jFlowNext", // must be class, use . sign
            auto: true	
        });
    
    },
    
    complete: function() {
        var menu = index.param("menu");
        menu = (menu == "null" ? "index" : (menu.length > 0 ? menu : "index"));
        $("#menu ul li a[rel='"+ menu +"'], #menu_lateral div a[rel='"+ menu +"']").parent().addClass("current");
        
        if ($("#audio_jplayer").length > 0) {
            index.audio();
        }
        
        if ($("#video_jplayer").length > 0) {
            index.video();
        }
        
        
        $('form#formulario #author, form#formulario #comment').blur(function() {
            $(this).removeClass('valid');
            if ($(this).val() == ''){
                $(this).addClass('error');
            } else {
                $(this).removeClass('error').addClass('valid');
            }
        });
        
        $('form#formulario #email').blur(function() {
            $(this).removeClass('valid');
            if (!index.is_email($(this).val())) {
                $(this).addClass('error');
            } else {
                $(this).removeClass('error').addClass('valid');
            }
        });
        
        $("form#formulario").submit(function() {
            $('form#formulario #author, form#formulario #email, form#formulario #comment').blur();
            if ($('form input.error').length == 0) {
                if ($('form input.valid').length > 0) {
                    return true;
                }
            }
            return false;
        });
        
        if ($("#pagina_total").length > 0) {
            index.paginacao();
        }
        
        if ($("#myController").length > 0) {
            index.slider();
        }
    }

}

$(document).ready(function() {
    index.complete();
});